# Spring Boot Application

## Overview

This Spring Boot application demonstrates a RESTful API with CRUD operations for employee data management. It includes Swagger for API documentation and uses EhCache for caching.

## Prerequisites

- Java JDK 8 or above
- Maven 3.6.0 or above
- EhCache

## Project Structure

- `.settings/`: IDE-specific settings for Eclipse.
- `logs/`: Application logs.
- `pom.xml`: Maven configuration file.
- `sqlEhCache/`: Local EhCache files.
- `src/`: Source files for the application.
  - `main/java/com/springboot/server/`: Application's Java source files, including the main application, configuration, controllers, and beans.

## Setup and Installation

```bash
git clone [repository-url]
cd [local-repository]
mvn install
```

## Running the Application

```bash
mvn spring-boot:run
```

## API Documentation

Access the Swagger UI at `http://localhost:8080/swagger-ui.html` to view and interact with the API's resources.

## Caching

The application uses EhCache to improve performance. Cache files are stored under `sqlEhCache/`.
