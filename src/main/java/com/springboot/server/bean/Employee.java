package com.springboot.server.bean;

import java.sql.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.stereotype.Component;

import com.opencsv.bean.CsvBindByPosition;


@Component
public class Employee {
	
	@CsvBindByPosition(position = 1)
	@NotNull
	private int id;
	@CsvBindByPosition(position = 2)
	private String height;
	@CsvBindByPosition(position = 3)
	private String weight;
	@CsvBindByPosition(position = 4)
	@NotNull
	@Pattern(regexp = "^[a-zA-Z0-9]{1,16}-[a-zA-Z0-9]{1,16}$", message = "The English name is not filled in or the format is incorrect")
	private String engName;
	@CsvBindByPosition(position = 5)
	@NotNull
	private String chnName;
	@CsvBindByPosition(position = 6)
	@NotNull
	@Digits(integer=4,fraction=0)
	private String ext ;
	@CsvBindByPosition(position = 7)
	@NotNull
	@Email(message = "\n" + 
			"Unfilled message or format is incorrect")
	private String email;
	@Digits(integer=2,fraction=6)
	private float bmi;
	private Date createtime;
	private Date updatetime;
	
	public Employee() {
		
	}
	public Employee(int Id,String Height,String Weight,String EName,String CName,String Ext,String Email,float BMI) {
		this.id=Id;
		this.height=Height;
		this.weight=Weight;
		this.engName=EName;
		this.chnName=CName;
		this.ext=Ext;
		this.email=Email;
		this.bmi=BMI;
	}
	public float getBMI() {
		return bmi;
	}
	public void setBMI(float bMI) {
		this.bmi = bMI;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getengName() {
		return engName;
	}
	public void setengName(String eName) {
		this.engName = eName;
	}
	public String getchnName() {
		return chnName;
	}
	public void setchnName(String cName) {
		this.chnName = cName;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String toString() {
		return "Employee[ID :" +  id+ ", Height:" +height  + ", Weight:" + weight + ", EName:" + engName +", CName:" + chnName +", Ext:" + ext +", Email:" + email +", BMI:" + bmi + ",Createtime:"+createtime+",Updatetime:"+updatetime+"]"+"\n";
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	
	
}