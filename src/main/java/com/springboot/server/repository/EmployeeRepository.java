package com.springboot.server.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.server.bean.Employee;

@Repository
public class EmployeeRepository {
	private static final Logger logger = LogManager.getLogger(EmployeeRepository.class);
	private final String insertSQl = "INSERT INTO hw2 (dataID,ID,Height,Weight,ENGname,CHNname,Ext,Email,BMI) VALUES (?,?,?,?,?,?,?,?,?)";
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	public Employee findById(int id) {
		String sql = "select * from hw2 where id =?";
		RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<>(Employee.class);
		Employee employee = jdbcTemplate.queryForObject(sql, rowMapper, id);
		return employee;
	}

	public int insert(Employee employee) {
		String sql = "insert into hw2 (ID,Height,Weight,ENGname,CHNname,Ext,Email,BMI)"
				+ " VALUES (:id,:height,:weight,:engName,:chnName,:ext,:email,:bmi)";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", employee.getId());
		params.addValue("height", employee.getHeight());
		params.addValue("weight", employee.getWeight());
		params.addValue("engName", employee.getengName());
		params.addValue("chnName", employee.getchnName());
		params.addValue("ext", employee.getExt());
		params.addValue("email", employee.getEmail());
		params.addValue("bmi", (Float.parseFloat(employee.getWeight())
				/ (Float.parseFloat(employee.getHeight()) * Float.parseFloat(employee.getHeight()) / 10000)));
		return namedJdbcTemplate.update(sql, params);
	}

	public Employee update(Employee employee) {
		logger.info("employee:{}",employee);
		String sql = "update hw2 set ID=:id,Height=:height,Weight=:weight,ENGname=:engName,CHNname=:chnName,Ext=:ext,Email=:email,BMI=:bmi where ID = :id";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", employee.getId());
		params.addValue("height", employee.getHeight());
		params.addValue("weight", employee.getWeight());
		params.addValue("engName", employee.getengName());
		params.addValue("chnName", employee.getchnName());
		params.addValue("ext", employee.getExt());
		params.addValue("email", employee.getEmail());
		params.addValue("bmi", (Float.parseFloat(employee.getWeight())
				/ (Float.parseFloat(employee.getHeight()) * Float.parseFloat(employee.getHeight()) / 10000)));
		namedJdbcTemplate.update(sql, params);
		logger.info("employee:{}",employee);
		return employee;

	}

	public int delete(int id) {
		String sql = "delete from hw2 where id=:id";
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
		namedJdbcTemplate.update(sql, namedParameters);
		return namedJdbcTemplate.update(sql, namedParameters);
	}

	@Transactional
	public void batchInsert(List<Employee> employeeData) throws SQLException {
		jdbcTemplate.batchUpdate(insertSQl, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
				if (i + 1 % 10 == 0) {
					logger.info("index :{}", i + 1);
					preparedStatement.executeBatch();
				}
				preparedStatement.setInt(2, employeeData.get(i).getId());
				preparedStatement.setString(3, employeeData.get(i).getHeight());
				preparedStatement.setString(4, employeeData.get(i).getWeight());
				preparedStatement.setString(5, employeeData.get(i).getengName());
				preparedStatement.setString(6, employeeData.get(i).getchnName());
				preparedStatement.setString(7, employeeData.get(i).getExt());
				preparedStatement.setString(8, employeeData.get(i).getEmail());
				preparedStatement.setFloat(9, employeeData.get(i).getBMI());
			}

			public int getBatchSize() {
				return employeeData.size();
			}

		});
	}
	public List<Employee> fileDownload() {
		String sql = "select * from hw2 ";
		RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<>(Employee.class);
		List<Employee> employee = jdbcTemplate.query(sql, rowMapper);
		logger.info("employee size :", employee.size());
		return employee;
	}

	public boolean checkId(int id) {
		String sql = "select count(*) from hw2 where id =?";
		return jdbcTemplate.queryForObject(sql,  new Object[] { id }, Integer.class) > 0;
	}
	public boolean checkExt(String ext) {
		String sql = "select count(*) from hw2 where Ext =?";
		return jdbcTemplate.queryForObject(sql,  new Object[] { ext }, Integer.class) > 0;
	}
	public boolean checkEmail(String email) {
		String sql = "select count(*) from hw2 where Email =?";
		return jdbcTemplate.queryForObject(sql,  new Object[] { email }, Integer.class) > 0;
	}
}
