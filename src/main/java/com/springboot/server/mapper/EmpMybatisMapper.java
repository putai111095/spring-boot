package com.springboot.server.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.server.bean.Employee;
public interface EmpMybatisMapper {
	 Employee findById(int id);
	 int insert(Employee employee); 
	 int update(Employee employee); 
	 int delete(int id);
	 @Transactional(rollbackFor = {RuntimeException.class})
	 void batchInsert(@Param("employeeList")List<Employee>list);
	 List<Employee> findAll();
}
