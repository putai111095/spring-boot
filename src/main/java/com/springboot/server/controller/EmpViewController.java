package com.springboot.server.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springboot.server.bean.Employee;
import com.springboot.server.service.EmployeeService;

@Controller
@RequestMapping("/view")
public class EmpViewController {
	private static final Logger logger = LogManager.getLogger(EmpViewController.class);
	@Autowired
	private EmployeeService service;

	@GetMapping(value = "/")
	public String findById(Model model) {
		// logger.info("{}",service.findAll().size());
		model.addAttribute("employee", service.findAll());
		return "index";
	}

	@GetMapping("/post")
	public String insertByPost(Employee employee) {
		return "addEmp";
	}

	@PostMapping("/add")
	public String addStudent(@Valid Employee employee, BindingResult result, Model model) {
		if (result.hasErrors()) {
            return "addEmp";
        }

		service.insert(employee);
		return "redirect:/view/";
	}

	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") int id, Model model) {
		
		Employee employee = service.findById(id);
		model.addAttribute("employee", employee);
		return "editEmp";
	}

	@PostMapping("/update")
	public String updateEmployee(@Valid Employee employee, BindingResult result, Model model) {
		 if (result.hasErrors()) {
	            return "editEmp";
	        }
		service.update(employee);
		model.addAttribute("students", service.findAll());
		return "redirect:/view/";
	}

	@GetMapping("/delete/{id}")
	public String deleteEmployee(@PathVariable("id") int id, Model model) {
		service.delete(id);
		model.addAttribute("employee", service.findAll());
		return "index";
	}
	@GetMapping("/findByid")
	public String showEmployeeById(@RequestParam (value = "search", required = false) int id, Model model) {
	    model.addAttribute("search", service.findById(id));
	    return "index";
	}
	@GetMapping("/login")
    public String login() {
        return "login";
    }
	
	
}
