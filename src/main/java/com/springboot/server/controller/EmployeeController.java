package com.springboot.server.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.springboot.server.bean.Employee;
import com.springboot.server.service.EmployeeService;

@RestController
@RequestMapping("/rest")
public class EmployeeController {
	private static final Logger logger = LogManager.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeService service;

//	@RequestMapping("/")
//	public String index() {
//		return service.findById(1);
//	}

	@GetMapping(value = "/findById")
	public Employee findById(@RequestParam int id) {
		return service.findById(id);
	}

	@PostMapping("/post")
	public int insertByPost(@Valid @RequestBody Employee employee) {
		return service.insert(employee);
	}

	@PutMapping("/update")
	public Employee updateResByPut(@Valid @RequestBody Employee employee) {
		service.update(employee);
		return employee;
	}

	@DeleteMapping(value = "/deleteById")
	public int deleteById(@RequestParam int id) {
		return service.delete(id);
	}

	@PostMapping("/upload")
	public void fileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		List<Employee> EmployeeData = new ArrayList<Employee>();
		if (file.isEmpty()) {
			logger.error("failed upload!");
		}
		try {
			InputStream inputStream = new BufferedInputStream(file.getInputStream());
			BufferedReader read = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
			String line = null;
			while ((line = read.readLine()) != null) {
				String[] tokens = line.split(",");
				Employee temp = new Employee(Integer.parseInt(tokens[0]), tokens[1], tokens[2], tokens[3], tokens[4],
						tokens[5], tokens[6], Float.parseFloat(tokens[8]));
				EmployeeData.add(temp);
			}
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		logger.info("success convert EmployeeData");

		service.batchinsert(EmployeeData);
	}

	@RequestMapping("/download")
	public void fileDownload(HttpServletResponse response) {
		String filename = "employee.csv";
		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"");
		// create a csv writer
		StatefulBeanToCsv<Employee> writer = null;
		try {
			writer = new StatefulBeanToCsvBuilder<Employee>(response.getWriter())
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withSeparator(CSVWriter.DEFAULT_SEPARATOR)
					.withOrderedResults(false).build();
		} catch (IOException e1) {
			logger.error("IOException:{}", e1);
		}
		// write all employees to csv file
		try {
			writer.write(service.fileDownload());
		} catch (CsvDataTypeMismatchException e) {
			logger.error("CsvDataTypeMismatchException:{}", e);
		} catch (CsvRequiredFieldEmptyException e) {
			logger.error("CsvRequiredFieldEmptyException:{}", e);
		}

	}

	@GetMapping("/checkId")
	public boolean checkId(@RequestParam int id) {
		return service.checkId(id);
	}
	@GetMapping("/checkExt")
	public boolean checkExt(@RequestParam String ext) {
		return service.checkExt(ext);
	}
	@GetMapping("/checkEmail")
	public boolean checkEmail(@RequestParam String email) {
		return service.checkEmail(email);
	}
	@GetMapping("/submitcheck")
	public boolean submitcheck(@RequestParam int id ,String ext,String email) {
		if (service.checkId(id)||service.checkExt(ext)||service.checkEmail(email)) {
			return true;
		}else  {
			return false;
		}
		
	}

}
