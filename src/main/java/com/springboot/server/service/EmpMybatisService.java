package com.springboot.server.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.springboot.server.bean.Employee;
import com.springboot.server.mapper.EmpMybatisMapper;
@Service
public class EmpMybatisService {
	private static final Logger logger = LogManager.getLogger(EmpMybatisService.class);
	@Autowired
	private EmpMybatisMapper empmybatismapper;
	@LogExecutionTime
	public Employee findById(int id) {
		return empmybatismapper.findById(id);
	}
	public int insert(Employee employee) {
		return empmybatismapper.insert(employee);	
	}
	public int update(Employee employee) {
		return empmybatismapper.update(employee);	
	}
	@LogExecutionTime
	@CacheEvict(value="empCache", key="#id")
	public int delete(int id) {
		return empmybatismapper.delete(id);
	}
	public void batchInsert(List<Employee> employeeData) {
		empmybatismapper.batchInsert(employeeData);
	}
	public List<Employee> findAll() {
		return empmybatismapper.findAll();
	}
	
}
