package com.springboot.server.service;

import java.sql.SQLException;
import java.util.List;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.springboot.server.bean.Employee;
import com.springboot.server.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository repository;

	@LogExecutionTime
	@Cacheable(value="empCache", key="#id")
	public Employee findById(int id) {
		return repository.findById(id);
	}
	@LogExecutionTime
	public int insert(Employee employee) {
		return repository.insert(employee);
	}
	@LogExecutionTime
	public Employee update(Employee employee) {
		repository.update(employee);
		return employee;
	}
	@LogExecutionTime
	@CacheEvict(value="empCache", key="#id")
	public int delete(int id) {
		return repository.delete(id);
	}
	@LogExecutionTime
	@CachePut(value="empCache", key="#employeeData")
	public void batchinsert(List<Employee> employeeData) {
		try {
			repository.batchInsert(employeeData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@LogExecutionTime
	public List<Employee> fileDownload() {
		return repository.fileDownload();
	}
	public List<Employee> findAll(){
		return repository.fileDownload();
	}
	public boolean checkId(int id) {
		return repository.checkId(id);
	}
	public boolean checkExt(String ext) {
		return repository.checkExt(ext);
	}
	public boolean checkEmail(String email) {
		return repository.checkEmail(email);
	}

}
